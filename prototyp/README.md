# 5. Prototyp

## Skapa en klickbar prototyp

* Använd Adobe XD
* Använd UI-kit, tex Bootstrap
* Skapa realistisk design för varje webbsida
* Gör knappar och länkar klickbara

## Exempel

![](https://lh6.googleusercontent.com/hgRmHcD06hF8ciPErWvHY7YhYU4ZyM0PkU9oA9tkHoW2LFGO2WYRFDOmDLuaE4kmTc0PMbZpSZexl6JsstrLdL2JBb4eBR7YSKSCpoN1HcVZLoHAFxrL7R-D2VD8BNBcROhqDuLgaYY)

## Generella designprinciper

### Layout och komposition

{% embed url="https://youtu.be/a5KYlHNKQB8" %}

### Några för användarvänlig design

{% embed url="https://youtu.be/D4W0BIRF41Y" %}

## Övningar

{% embed url="https://scrimba.com/learn/design" %}
