# Vad är gränssnittsdesign?

## UX vs UI

{% embed url="https://www.youtube.com/watch?v=lQqd7x_6GwE&feature=youtu.be" %}

## Webb utan UX

{% embed url="https://youtu.be/Ojiv9Smi4XE" %}

## Inspiration

* Formgivaren Dieter Rams användarcentrerad design har inspirerat många, däribland Apple

{% embed url="https://youtu.be/-N5aQyCzm4I" %}

{% embed url="https://youtu.be/xrhNIU-BLZw" %}

