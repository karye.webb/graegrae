# 2. Moodboard

## **Skapa en moodboard**

* Googla moodboard
* Brainstorma - minst 20 ord som du kommer att tänka när du hör: "resa" och "dagbok"
* Ett collage med syfte att inspirera att synliggöra en röd tråd
* Samla 10-15 bilder hittat på Internet
  * Exemplifiera färger
  * Exemplifiera form
  * Exemplifiera typsnitt
  * Exemplifiera bilder kopplade till hemsidans tema

{% embed url="https://youtu.be/ERrDs9VSOq0" %}

{% embed url="https://moderskeppet.se/kurser/kurs/moodboard/vad-aer-en-moodboard-egentligen" %}

## Exempel

![](https://lh5.googleusercontent.com/jInyvJKRx\_cSILJO2hJCH401QvwFxn5HZPbL5RNLeQHPKji3epTBORzY9eK3EAKpxb5dfqAczgAkTk0FrRWnLKvkX3SScFn2QiBShVYZV2eF0fSfdqK-AiDNDqyoJOMv36F\_gnDivAs)
