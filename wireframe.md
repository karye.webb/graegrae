# 4. Wireframe

## Skapa en wireframe

* Använd [https://wireframe.cc](https://wireframe.cc)
* Skapa enkel wireframe för varje sida
* Öppna en ny wireframe.cc för varje webbsida du skall skissa
* Ta skärmdump på varje wireframe som du infogar i projektplanen

## Exempel

![](https://lh3.googleusercontent.com/MaObHp0VpBFDh8ocUFHDKTuS4KswHppojVceak5iHBbPXxmiMQxJnYsNot1m-Bzm6f36JSq0xw1gTBpL\_iuaZPXj\_lFkqeDiHQi9uYxyddfKRAPDGHN4\_ciNpdPisLbEWS2-sS4jBcY)
